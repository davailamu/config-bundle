**Installation**


```
"require" : {
      ...
      "mgd/config-bundle" : "dev-master"
  },
"repositories" : [{
    "type" : "vcs",
    "url" : "git@bitbucket.org:mgd-team/config-bundle.git"
}]

```

run `composer update` in terminal 
     
**Enable the bundle**

```
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            ...
            new MGD\ConfigBundle\MGDConfigBundle(),
        ];
        ...
    }
```
**Register namespaces**
```
twig:   
    paths:
        '%kernel.project_dir%/vendor/mgd/config-bundle/Resources/views': MGDConfigBundle
        '%kernel.project_dir%/vendor/sonata-project/admin-bundle/src/Resources/views': SonataAdminBundle
```

**Configure the MGDConfigBundle**

The following configuration is optional

```
mgd_config:
      admin_class:  // sonata admin class; default: MGD\ConfigBundle\Admin\ConfigAdmin
```

Also `friendsofsymfony/ckeditor-bundle` for Symfony 4/5 is required

Enable admin class:

```
sonata_admin:
    dashboard:
        groups:
            Config:
                label: ~
                label_catalogue: ~
                on_top: true
                icon: '<i class="fa fa-cogs"></i>'
                items:
                    - mgd_config.admin.config
```

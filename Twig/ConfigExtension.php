<?php

namespace MGD\ConfigBundle\Twig;

use MGD\ConfigBundle\Factory\ValueConfiguratorFactory;
use MGD\ConfigBundle\Model\BaseConfig;
use MGD\ConfigBundle\Service\ConfigManager;
use MGD\ConfigBundle\Model\ConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ConfigExtension extends AbstractExtension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * ConfigExtension constructor.
     *
     * @param ConfigManager $configManager
     */
    public function __construct(ConfigManager $configManager, ContainerInterface $container)
    {
        $this->configManager = $configManager;
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return array(
            new TwigFilter('get_by_code', array($this, 'getByCode')),
            new TwigFilter('get_name_by_code', array($this, 'getNameByCode')),
            new TwigFilter('get_value_by_code', array($this, 'getValueByCode')),
            new TwigFilter('get_template', array($this, 'getTemplate')),
        );
    }

    /**
     * @param string $code
     * @return ConfigInterface|null
     */
    public function getByCode(string $code): ?ConfigInterface
    {
        return $this->configManager->getByCode($code);
    }

    /**
     * @param string $code
     * @return string
     */
    public function getNameByCode(string $code): string
    {
        $config = $this->configManager->getByCode($code);

        if ($config) {
            return $config->getName();
        }

        throw new \RuntimeException("Config entity with code: $code does not exist");
    }

    /**
     * @param string $code
     * @param bool   $isHtml
     * @return string
     */
    public function getValueByCode(string $code, $isHtml = true): ?string
    {
        $config = $this->configManager->getByCode($code);

        if ($config) {
            return ($isHtml) ? $config->getValue() : strip_tags($config->getValue());
        }

        throw new \RuntimeException("Config entity with code: $code does not exist");
    }

    /**
     * @param BaseConfig $config
     * @return string
     */
    public function getTemplate($config)
    {
        $factory = $this->container->get(ValueConfiguratorFactory::class);
        $configurator = $factory->create($config);

        return $configurator->getTemplate();
    }
}

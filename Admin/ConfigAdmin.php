<?php

namespace MGD\ConfigBundle\Admin;

use MGD\ConfigBundle\Factory\ValueConfiguratorFactory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Form\Type\Operator\ContainsOperatorType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigAdmin extends AbstractAdmin
{
    /** @var ValueConfiguratorFactory */
    private $configuratorFactory;

    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        ValueConfiguratorFactory $configuratorFactory
    ) {
        parent::__construct($code, $class, $baseControllerName);

        $this->configuratorFactory = $configuratorFactory;
    }

    protected function configureDefaultFilterValues(array &$filterValues): void
    {
        $filterValues['name'] = [
            'type' => ContainsOperatorType::TYPE_CONTAINS,
        ];
    }

    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('name')
            ->add('value', null, array(
                'template' => '@MGDConfigBundle/list_field.html.twig'
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('name');

        $configurator = $this->configuratorFactory->create($this->getSubject());
        $configurator->configureFormFields($form, $this);
    }

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->clearExcept(array('edit', 'list'));
    }
}

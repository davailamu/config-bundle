<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.09.17
 * Time: 6:05
 */

namespace MGD\ConfigBundle\Admin\Configurator;

use MGD\ConfigBundle\Model\StringConfig;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class StringConfigurator implements ValueAdminConfiguratorInterface
{
    public function configureFormFields(FormMapper $formMapper, AbstractAdmin $context)
    {
        $formMapper
            ->add('value', TextType::class);
    }

    public static function getTarget(): string
    {
        return StringConfig::class;
    }

    public static function getTemplate(): string
    {
        return '@MGDConfigBundle/default_value_field.html.twig';
    }
}

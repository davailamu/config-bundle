<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.09.17
 * Time: 6:05
 */

namespace MGD\ConfigBundle\Admin\Configurator;

use MGD\ConfigBundle\Model\HtmlConfig;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class HtmlConfigurator implements ValueAdminConfiguratorInterface
{
    public function configureFormFields(FormMapper $formMapper, AbstractAdmin $context)
    {
        $formMapper
            ->add('value', $this->getCKEditorTypeClass());
    }

    public static function getTarget(): string
    {
        return HtmlConfig::class;
    }

    public static function getTemplate(): string
    {
        return '@MGDConfigBundle/html_value_field.html.twig';
    }

    // to maintain projects with diff CKEditor types
    private function getCKEditorTypeClass()
    {
        if (class_exists('FOS\CKEditorBundle\Form\Type\CKEditorType')) {
            return 'FOS\CKEditorBundle\Form\Type\CKEditorType';
        }

        return 'Ivory\CKEditorBundle\Form\Type\CKEditorType';
    }
}

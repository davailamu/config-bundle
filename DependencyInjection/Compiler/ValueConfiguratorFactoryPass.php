<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 8/4/17
 * Time: 10:49 AM.
 */

namespace MGD\ConfigBundle\DependencyInjection\Compiler;

use MGD\ConfigBundle\Factory\ValueConfiguratorFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ValueConfiguratorFactoryPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $factoryDefinition = $container->getDefinition(ValueConfiguratorFactory::class);
        $services = $container->findTaggedServiceIds('mgd_config.value_admin_configurator');

        foreach ($services as $id => $tag) {
            $factoryDefinition->addMethodCall('addConfigurator', [new Reference($id)]);
        }
    }
}

<?php

namespace MGD\ConfigBundle\Model;

use MGD\ConfigBundle\Traits\ConfigValueTrait;

class IntegerConfig extends BaseConfig
{
    use ConfigValueTrait;

    /**
     * @var integer
     */
    protected $id;
}

<?php

namespace MGD\ConfigBundle\Model;

use MGD\ConfigBundle\Traits\ConfigValueTrait;

class TextConfig extends BaseConfig
{
    use ConfigValueTrait;

    /**
     * @var integer
     */
    protected $id;
}

<?php

namespace MGD\ConfigBundle\Model;

use MGD\ConfigBundle\Traits\ConfigValueTrait;

class BooleanConfig extends BaseConfig
{
    use ConfigValueTrait;

    /**
     * @var integer
     */
    protected $id;
}

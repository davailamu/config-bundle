<?php

namespace MGD\ConfigBundle\Model;

use MGD\ConfigBundle\Traits\ConfigValueTrait;

class HtmlConfig extends BaseConfig
{
    use ConfigValueTrait;

    /**
     * @var integer
     */
    protected $id;

    /**
     * HtmlConfig constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        if (!class_exists('FOS\CKEditorBundle\Form\Type\CKEditorType') && !class_exists('Ivory\CKEditorBundle\Form\Type\CKEditorType')) {
            throw new \Exception('Class CKEditorType not found.  Please use or https://symfony.com/doc/current/bundles/IvoryCKEditorBundle/index.html
                either https://symfony.com/doc/current/bundles/FOSCKEditorBundle/index.html');
        }
    }
}

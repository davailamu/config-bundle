<?php

namespace MGD\ConfigBundle\Model;

use MGD\ConfigBundle\Traits\ConfigValueTrait;

class StringConfig extends BaseConfig
{
    use ConfigValueTrait;

    /**
     * @var integer
     */
    protected $id;
}

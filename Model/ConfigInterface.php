<?php

namespace MGD\ConfigBundle\Model;

interface ConfigInterface
{
    /**
     * Set code.
     *
     * @param string $code
     *
     * @return BaseConfig
     */
    public function setCode(string $code): BaseConfig;

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode(): string;

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return BaseConfig
     */
    public function setName(string $name): BaseConfig;

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param $value
     * @return BaseConfig
     */
    public function setValue($value): BaseConfig;
}

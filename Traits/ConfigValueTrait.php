<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.09.17
 * Time: 12:28
 */

namespace MGD\ConfigBundle\Traits;

use MGD\ConfigBundle\Model\BaseConfig;

trait ConfigValueTrait
{
    protected $value;

    public function setValue($value) : BaseConfig
    {
        $this->value = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }
}